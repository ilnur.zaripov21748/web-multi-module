"use client"

import {DashboardAdmin} from "@admin/feature-dashboard";
import {DashboardUser} from "@user/feature-dashboard";
import {AppPage, NotFoundPage, PageRole, Role} from "@core/component";

export default function Dashboard() {
    return <AppPage roleMap={[
        new PageRole(Role.USER, <DashboardUser/>),
        new PageRole(Role.ADMIN, <DashboardAdmin/>),
        new PageRole(Role.ANON, <NotFoundPage/>),
    ]}/>
}