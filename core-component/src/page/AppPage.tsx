import React, {useEffect, useState} from "react";
import {AppPageProps, PageRole, Role} from "./base-page-setting";
import {LoadingPage, NotFoundPage} from "../component";
import {NetworkProviderImpl} from "@core/network";

type Props = Readonly<AppPageProps>

export function AppPage(props: Props) {

    const provider = new NetworkProviderImpl()

    const [user, setUser] = useState({role: Role.ANON, component: <LoadingPage/>})


    useEffect(() => {
        const auth1 = auth()
        setUser({role: auth1.role, component: auth1.component})
    }, []);

    function getRole(): Role {
        return Role.ADMIN
    }

    //
    function auth() {
        provider.getRole();
        let role = getRole()
        return props.roleMap.find((item) => item.role === role) ??
            new PageRole(
                Role.ANON,
                // eslint-disable-next-line react/jsx-no-undef
                <NotFoundPage/>
            )
    }

    return (
        user.component
    )
}

// export type Props = Readonly<AppPageProps>
// export type State = Readonly<AppPageState>
//
// export class AppPage extends React.Component<Props, State> {
//
//     constructor(props: Props) {
//         super(props)
//         this.state = {role: Role.ANON, component: <LoadingPage/>}
//     }
//
//     getRole(): Role {
//         return Role.ADMIN
//     }
//
//     private async auth() {
//         let role = this.getRole()
//         return this.props.roleMap.find((item) => item.role === role) ??
//             new PageRole(
//                 Role.ANON,
//                 // eslint-disable-next-line react/jsx-no-undef
//                 <NotFoundPage/>
//             )
//     }
//
//     async componentDidMount() {
//         const auth = await this.auth()
//         this.setState({role: auth.role, component: auth.component})
//     }
//
//     render() {
//         return this.state.component;
//     }
//
//
// }
