import {JSX} from "react";

export interface AppPageProps {
    roleMap: Array<PageRole>
}

export interface AppPageState {
    role: Role,
    source: JSX.Element | void
}

export class PageRole {

    constructor(
        readonly role: Role,
        readonly component: JSX.Element
    ) {
    }
}

export enum Role {
    USER, ADMIN, ANON
}