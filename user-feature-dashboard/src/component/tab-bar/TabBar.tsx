import React from "react";

export function TabBar() {
    return <div className="flex flex-row gap-x-2">
        <div>На главную</div>
        <div>Мои грузы</div>
        <div>Мои заявки</div>
    </div>
}