import React from "react";
import {TabBar} from "../tab-bar/TabBar";

export function DashboardUser() {
    return <div className="flex flex-col gap-y-2">
        <TabBar></TabBar>
        <div>
            Панель управления Пользователя
        </div>
    </div>
}
