import React from "react";
import {TabBar} from "../tab-bar/TabBar";

export function DashboardAdmin() {
    return <div className="flex flex-col gap-y-2">
        <TabBar></TabBar>
        <div>
            Панель управления адмнистратором
        </div>
    </div>
}
